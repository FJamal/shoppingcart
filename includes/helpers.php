<?php

	//starting session
	session_start();

	error_reporting(E_ALL);
	ini_set("display_errors", 1);


	/*function that takes in the value of id for category in menu.xml
	and returns an associative array with two keys categoryname and options, the
	later key is refrenced to an array containing associative arrays*/
	function getmenu($id)
	{
		//loading xml file
		$dom = simplexml_load_file("../menu.xml");

		//array to store data
		$data = [];

		//loading that particular category based on id number
		foreach($dom->xpath("/menu/category[@id = \"{$id}\"]") as $category)
		{
			//adding the name of the category
			//array_push($data, ["categoryname" => (string)$category->option]);
			$data["categoryname"] = (string)$category->option;

			/*creating an empty array for a key so that name,sizes can be stored as
			a single array for this key*/
			$data["options"] = [];

			foreach ($category->name as $options)
			{
				//to store all options
				$optionnames = [];
				$optionname = (string)$options->type;

				//array_push($optionnames, ["name" => $optionname]);
				$optionnames["name"] = $optionname;



				//if size small is set or large or general
				if(!empty($options->small))
				{
					$smallprice = (string)$options->small;
					//array_push($optionnames, ["small" => $smallprice]);
					$optionnames["small"] = $smallprice;
				}
				if(!empty($options->large))
				{
					$largeprice = (string)$options->large;
					//array_push($optionnames, ["large" => $largeprice]);
					$optionnames["large"] = $largeprice;
				}
				if(!empty($options->general))
				{
					$generalprice = (string)$options->general;
					//array_push($optionnames, ["general" => $generalprice]);
					$optionnames["general"] = $generalprice;
				}

				array_push($data["options"], $optionnames);
			}
		}

		return $data;
	}
	/*function that sets up the pagination links in the footer
	takes in the page number as an argument referenced from index.php as $_GET["page"]*/
	function makepagination($pagenumber)
	{
		// flags to dertmine which li tag to disable in footer.php
		/*$flagpreviouspage = true;
		$flagnextpage = true;*/

		/*flags and variable for links global so that they can be used
		by footer.php for pagination */
		global $flagpreviouspage;
		global $flagnextpage;
		global $linkprevious;
		global $linknext;
		$flagpreviouspage = true;
		$flagnextpage = true;

		$dom = simplexml_load_file("../menu.xml");

		//to count the number of categories
		$countercat = 0;
		foreach($dom->xpath("/menu/category") as $category)
		{
			$countercat += 1;
		}

		//checking if the page number is > 1 and < max number of categories in xml and applying pagination links
		if ($pagenumber > 1 && $pagenumber < $countercat)
		{
			$previouspage = $pagenumber - 1;
			$nextpage = $pagenumber + 1;

			$linkprevious = "?page={$previouspage}";
			$linknext = "?page={$nextpage}";
		}
		else if($pagenumber == 1)
		{
			$flagpreviouspage = false;
			$nextpage = $pagenumber + 1;
			$linknext = "?page={$nextpage}";
			$linkprevious = "";

		}
		else if ($pagenumber == $countercat)
		{
			$flagnextpage = false;
			$previouspage = $pagenumber - 1;
			$linkprevious = "?page={$previouspage}";
			$linknext = "";
		}
	}

	/* function that loads the appropriate page based on $-GET["page"]
	takes in pagenumber from index.php as argument */
	function loadpage($pagenumber)
	{
		//getting the menu based on pagenumber
		$data = getmenu($pagenumber);

		//calling the pagination accordingly
		makepagination($pagenumber);

		//loading the views
		require("../views/header.php");
		require("../views/menu.php");
		require("../views/footer.php");
	}

	/*Function to get the appropriate image link for the menu*/
	function getimage($pagenumber)
	{
		$images = ["../images/pizza.png",
					"../images/sideorders.jpg",
					"../images/special.jpg",
					"../images/salads.jpg",
					"../images/spaghetti.jpg",
					"../images/ravioli.jpg",
					"../images/Calzone.png"];

		// minusing 1 cuz array indexed from 0
		return $images[$pagenumber - 1];
	}


	/*Function to redirect to certain pages*/
	function redirect($page)
	{
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$page = $page . ".php";
		print($page);
		header("Location: http://$host$uri/$page");
		exit;
		break;
	}


	/*function used by cart.php that takes in the value of the radio
	buttton splits that value and serach for its price in xml and return an
	appropriate dictionary */
	function getprice($value, $location)
	{

		//split the first half of value b4 "#"
		$name = strstr($value, "#", true);
		//print($name."\n");

		//finding the position of "#" in $value
		$position = strpos($value, "#");
		//print($position."\n");

		//getting the part after "#" to get what size the item was bought, >Documentation of function
		$size = substr($value, $position + 1);
		//print($size."\n");

		/*find the position of "=" in $location and get category number
		from page=number*/
		if($location == "http://cart.local/" || $location == "http://cart.local/index.php")
		{
			//fixing bug ie item bought from index.php
			$id = 1;
		}
		else
		{
			$id = substr($location, strpos($location, "=") + 1);
		}
		//print($id ."\n");

		$price;
		$menutype;
		//getting its price by comparing $name and $size appropraitely
		$dom = simplexml_load_file("../menu.xml");

		foreach($dom->xpath("/menu/category[@id = \"{$id}\"]") as $category)
		{
			$menutype = (string)$category->option;

			foreach($category->name as $item)
			{
				if ($item->type == $name)
				{
					$price = (string)$item->{$size};
				}
			}
		}

		/*returning a- array with key as value of radio button so to trace
		multiple orders of the same thing */
		return ["menu" => $menutype,
				"name" => $name,
				"size" => $size,
				"price" => $price,
				"key" => $value,
				"quantity" => 1];

	}

	/*Function to count the total number of items in cart so that it could
	be used by header.php to display the number. It calculates the sum of all
	$_SESSION["cart"][index]["quantity"] and returns the sum*/
	function itemsincart()
	{
		if(isset($_SESSION["cart"]))
		{
			$sum = 0;
			$count = count($_SESSION["cart"]);

			//looping through each array
			for($i = 0; $i < $count; $i++)
			{
				$sum = $sum + $_SESSION["cart"][$i]["quantity"];
			}

			return $sum;
		}
		else
		{
			return "";
		}
	}


	/*function that calculates the total price of each item in the cart
	that is price x quantity of each item and reutrns the appropriate
	array*/
	function calculateprice()
	{
		$data = [];
		$total;
		if(isset($_SESSION["cart"]))
		{
			foreach($_SESSION["cart"] as $items)
			{
				$total = (float)$items["quantity"] * (float)$items["price"];
				//changing it to 2 decimal places
				$total = number_format($total, 2, ".", "");

				// adding the total price for each together with all the previous data
				$data []= ["menu" => $items["menu"],
						"name" => $items["name"],
						"size" => $items["size"],
						"price" => $items["price"],
						"key" => $items["key"],
						"quantity" => $items["quantity"],
						"total" => $total];
			}
			return $data;
		}
	}

	/*The $_POST keys when clicked in cart are displayed as
	 [Tomato_&_Cheese#large] instead of [Tomato & Cheese#large]
	 this function replaces all the "_" in keys with a space
	 so that the keys could be used to refer it to $_SESSION
	 so that quantity could be updated no argument since $_POST
	 is gloabl */
	 function replace_underscores()
	 {
		foreach($_POST as $key => $value)
		{
			//escaping the [update] key
			if($key != "update")
			{
				//replacing the underscores with space
				$newkey = str_replace("_", " ", $key);

				//creating a new key in $_POST
				$_POST[$newkey] = $value;

				/*fixing bug so that it doesnt delete the key if $_POST
				values doesnt have underscores in it*/
				if($newkey != $key)
				{
					//deleting the old key
					unset($_POST[$key]);
				}
			}
		}

 	}

	/*This upates the quantity supplied in $_POST from viewcart.php
	into the $_SESSIONS["cart"], if update value is 0, that category is
	removed*/
	function update()
	{

		foreach($_POST as $key => $value)
		{
			if($key != "update")
			{
				//to track $_SESSION["cart"] array index
				$count = 0;
				foreach($_SESSION["cart"] as $items)
				{
					//checking for match of keys in Sessions and POST
					if($items["key"] == $key)
					{
						//print("MATCH FOUND". "</br>");
						//print($items["key"]."</br>");
						//print($key."</br>");
						//now checking if value supplied is in a number form
						if(is_numeric($value))
						{
							//print($value."</br>");
							//further checking if it was not zero and is an int
							//type casting
							$value = (int)$value;
							if($value != 0 && is_int($value))
							{
								//updating quantity
								$_SESSION["cart"][$count]["quantity"] = $value;
								//print_r($_SESSION["cart"][$count] );
							}
							else if($value == 0)
							{
								//remove that item from session
								unset($_SESSION["cart"][$count]);
								//reindexing the $_SESSION[cart] array
								$_SESSION["cart"] = array_values($_SESSION["cart"]);
							}
						}
					}

					// increminting count
					$count += 1;
				}

			}

		}
	}

	/*Calculate the grand total of all the items that is all the sum
	of items["total"] in mycart.php*/
	function grandtotal($arr)
	{
		$sum = 0;
		foreach($arr as $item)
		{
			$sum = $sum + $item["total"];
			$sum = number_format($sum, "2", ".", "");
		}
		return $sum;
	}


	/*Tis is used by mycart.php when the user removes an item via clicking
	the X, it searches $_POST for a key that starts with "remove", than filters
	that key by removing the string "remove" and replacing "_" with a space
	and than use it to refer it to $_SESSION["cart"][index]["key"]
	and remove that item from sessions*/
	function remove()
	{
		$index;
		foreach($_POST as $key => $value)
		{
			//if the function doesnt return false means "remove" is in $key
			if(strpos($key, "remove")!== false)
			{
				print($key."</br>");
				/*since we know every name of "X" starts with "remove"
				hardcoding the position to extract the name from $key*/
				$name = substr($key, "6");

				//now replacing "_" with a space
				$name = str_replace("_", " ", $name);

				/*now tracking the element in $_SESSION[cart][index]
				that has the key equals to $name and deleting it*/
				$count = 0;

				foreach($_SESSION["cart"] as $items)
				{

					if($items["key"] == $name)
					{
						//record the index number
						$index= $count;
						break;
					}

					$count += 1;
				}
				break;
			}
		}

		//delete the $_SESSION[cart] array at index = $index
		unset($_SESSION["cart"][$index]);
		//reindexing
		$_SESSION["cart"] = array_values($_SESSION["cart"]);
	}

?>
