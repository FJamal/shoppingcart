<div class="menu">
    <?php if(isset($emptycartflag)): ?>
        <p>There are no items in your cart to display</p>
    <?php elseif(isset($checkoutflag)):?>
        <p>Thank you for ordering.Your order would be precessed shortly.(Well not Really!)</p>
    <?php else: ?>
        <div class="tab">
            <form action="../mycart.php" method="POST">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Menu</th>
                            <th>Option</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($items as $item): ?>
                            <tr>
                                <td><?= $item["menu"] ?></td>
                                <td><?= $item["name"] ?></td>
                                <td><?= $item["size"] ?></td>
                                <td>$<?= $item["price"] ?></td>
                                <td class="tablequantity">
                                    <div class= "col-xs-3">
                                        <input class="form-control" type="text" name="<?= $item["key"]?>" value="<?= $item["quantity"] ?>">
                                    </div>

                                </td>
                                <td>$<?= $item["total"] ?></td>
                                <!--name for each remove "X" = remove+key-->
                                <td class ="removeitem"><input class="btn btn-default" type="submit" value="X" name ="remove<?= $item["key"] ?>"></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="total">
                            <td colspan="5"><b>TOTAL</b></td>
                            <td colspan = "2"  id ="sum"><b>$<?= $total ?> </b></td>
                        </tr>
                    </tbody>

                </table>
                <div class= "submit">
                    <input class="btn btn-success" type="submit" value="Update" name ="update">
                    <input class="btn btn-success" type="submit" value="Checkout" name="checkout">
                </div>
            </form>

        </div>
    <?php endif; ?>

</div>
</body>
</html>
