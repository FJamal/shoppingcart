<div class="pagi">
    <ul class ="pager">
        <!--displaying next/previous buttons according to their flags from pagination function -->
        <?php if($flagpreviouspage && $flagnextpage): ?>
            <li><a href="<?= $linkprevious ?>">Previous</a></li>
            <li><a href="<?= $linknext ?>">Next</a></li>
        <!-- for first page -->
        <?php elseif($flagpreviouspage == false): ?>
            <li class = "disabled"><a href="<?= $linkprevious ?>">Previous</a></li>
            <li><a href="<?= $linknext ?>">Next</a></li>
        <!-- for last page -->
        <?php elseif($flagnextpage == false): ?>
            <li><a href="<?= $linkprevious ?>">Previous</a></li>
            <li class= "disabled"><a href="<?= $linknext ?>">Next</a></li>
        <?php endif; ?>
    </ul>
</div>

</body>
</html>
