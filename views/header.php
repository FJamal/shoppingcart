<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Shopping Cart</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
        <div class="headpanel">
            <div class = "cart">
                <!--To determine to display menu or the shopping cart, menuflag refered from mycart.php-->
                <?php if(isset($menuflag)): ?>
                    <a href="../index.php"><img src="../images/menu.png" alt="Visit Menu">Menu</a>
                <?php else: ?>
                    <a href="../mycart.php"><img src="../images/cart.png" alt="shopping cart"><?= $numberofitemsincart ?></a>
                <?php endif; ?>
            </div>
        </div>
        <?php if(isset($menuflag)): ?>
            <h1>My Cart</h1>
        <?php else: ?>
            <h1>Shopping Cart</h1>
        <?php endif; ?>
