<div class = "menu">
    <img src="<?= $image ?>" alt="Items Pic" class="img-circle" width="11%" height="11%">
    <h3><?= $data["categoryname"] ?></h3>
    <div class = "tab">
        <form action="../cart.php" method="POST">
            <table class = "table table-striped">
                <thead>
                    <tr>
                        <th>Options</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Buy</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($data["options"] as $items): ?>
                    <?php $flagforrow = false; ?>
                    <tr>

                            <!--checking if the count is > 2 so as to display
                            another row for large/ssmall size -->
                        <?php
                            if(count($items) > 2)
                            {
                                $flagforrow = true;
                            }

                         ?>

                        <td><?= $items["name"] ?></td>
                        <td>
                            <?php
                                if(isset($items["general"]))
                                {
                                    print("General");
                                }
                                else
                                {
                                    print("Small");
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(isset($items["general"]))
                                {
                                    print("$".$items["general"]);
                                }
                                else
                                {
                                    print("$".$items["small"]);
                                }
                            ?>
                        </td>
                        <?php
                            //to determine the value for the below radio button
                            $valuename;
                            if(isset($items["general"]))
                            {
                                $valuename = "general";
                            }
                            else
                            {
                                $valuename = "small";
                            }
                        ?>
                        <!-- Value is name of item seprated by "#" then its size to make easy tracing in xml -->
                        <td><input type="radio" name="item" value="<?= $items['name']?>#<?= $valuename?>"></td>
                    </tr>
                    <!-- checking flag for another row to display size large -->
                    <?php if($flagforrow): ?>
                        <tr>
                            <td><?= $items["name"] ?></td>
                            <td>Large</td>
                            <td>$<?= $items["large"] ?></td>
                            <td><input type="radio" name="item" value="<?= $items["name"]?>#large"></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>

                </tbody>
            </table>
            <div class= "submit">
                <input class="btn btn-success" type="submit" value="Buy!">
            </div>
        </form>
    </div>
</div>
