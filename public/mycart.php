<?php
    //configuration
    require("../includes/helpers.php");

    //to be used in header.php
    $menuflag = true;

    //if user visited via GET that is user wanted to view the cart
    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        /*if the cart is empty ***bug fix:The empty cart msg was not
        showing if items were bought and then removed via clicking
        X cuz initially it was here only checking !isset($_SESSION["cart"]
        added the or condition so what even if $_SESSION["cart"] was set but
        if empty than should display the empty cart msg */ 
        if(!isset($_SESSION["cart"]) || empty($_SESSION["cart"]))
        {
            $emptycartflag = true;
            require("../views/header.php");
            require("../views/viewcart.php");
        }
        else
        {
            //getting all the data for items in cart
            $items = calculateprice();

            //calculate grand total
            $total = grandtotal($items);

            require("../views/header.php");
            require("../views/viewcart.php");
        }
    }
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        //the user checked out
        if(isset($_POST["checkout"]))
        {
            $checkoutflag = true;
            session_destroy();
            require("../views/header.php");
            require("../views/viewcart.php");
        }
        //if update was clicked
        elseif (isset($_POST["update"]))
        {
            //correcting the names of keys so that it can be referred to $_SESSIONS
            replace_underscores();

            // updating the new quantities
            update();

            //redirecting to same page
            redirect("mycart");

        }
        // if a item is removed via clicking the X
        else
        {
            /*print("SESSIONS BEFORE");
            print("<pre>");
            print_r($_SESSION);
            print("</pre>");

            print_r($_POST);
            print("</br></br>");*/

            remove();

            /*print("SESSIONS AFTER");
            print("<pre>");
            print_r($_SESSION);
            print("</pre>");*/
            redirect("mycart");

        }
    }


?>
