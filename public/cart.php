<?php
    //configuration
    require("../includes/helpers.php");

    //work only if user reaches via POST otherwise direct to index.php
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        //to store what page the user bought an item from
        $location = $_SERVER['HTTP_REFERER'];

        //calling function to get its price
        $iteminfo = getprice($_POST["item"], $location);
        //print_r($iteminfo);

        /*if sessions is empty add the item thats been bought
        if its not empty look into all the items and see if a item with the same
        key is already in the cart if its there increment its quantity else add
        the item into sessions*/

        if(!isset($_SESSION["cart"]))
        {
            //if not set then declare that session["cart"] is an array of arrays
            $_SESSION["cart"] = [];
            array_push($_SESSION["cart"], $iteminfo);
        }
        else
        {
            $flagforamatch = false;
            $count = 0;
            //to track position in sessions as to where to increment quantity
            $sessionposition;

            foreach($_SESSION["cart"] as $items)
            {
                if($items["key"] == $_POST["item"])
                {
                    $flagforamatch = true;
                    //saving the array index
                    $sessionposition = $count;
                }
                $count += 1;
            }

            if($flagforamatch)
            {
                //incrementing quantity of that item
                $_SESSION["cart"][$sessionposition]["quantity"] += 1;
            }
            else
            {
                array_push($_SESSION["cart"], $iteminfo);
            }
        }

        //print_r($_SESSION);
        //then redirecting to the same page
        header("Location: $location");
    }
    else
    {
        redirect("index");
    }
 ?>
