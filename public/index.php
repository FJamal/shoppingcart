<?php
    //adding config file
    require("../includes/helpers.php");


    //checking if GET variable for page number is set
    if(isset($_GET["page"]))
    {
        $pagenumber = $_GET["page"];
    }
    else
    {
        $pagenumber = 1;
    }

    /*to display the number of items in the cart in the header*/
    $numberofitemsincart = itemsincart(); 

    switch ($pagenumber)
    {
        case '1':
            $data = getmenu($pagenumber);
            //calling to get the correct pagination
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            /*print("<pre>");
            print_r($data);
            print("</pre>");*/
            //loadpage($pagenumber);
            break;

        case "2":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        case "3":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        case "4":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        case "5":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        case "6":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        case "7":
            $data = getmenu($pagenumber);
            makepagination($pagenumber);

            //getting image link
            $image = getimage($pagenumber);

            require("../views/header.php");
            require("../views/menu.php");
            require("../views/footer.php");
            break;
        default:
            //redirecting to index.php
            $host  = $_SERVER['HTTP_HOST'];
            $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $extra = 'index.php';
            header("Location: http://$host$uri/$extra");
            exit;
            break;
    }
 ?>
