<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Test Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
        body
        {
            text-align: center;
        }
        .headpanel
        {
            background-color: red;
            width: 100%;
            height: 40px;

        }
        .cart img
        {
            height: 100%;
            vertical-align: baseline;

        }
        .cart a
        {
            font-size: 150%;
            text-decoration: none;

        }
        .cart
        {
            float: right;
            height: 100%;
            margin-right: 1.5%;
        }
        .menu
        {
            margin-top: 4%;
        }
        .tab
        {
            margin: 3% 10% 0 10%;
        }
        th
        {
            text-align: center;
        }
        .submit
        {
            float: right;
            margin-right: 3%;
        }
        .pagi
        {
            text-align: center;
            background-color: blue;
            margin: 6% auto 0 auto;
        }
        </style>
    </head>

    <body>
        <div class="headpanel">
            <div class = "cart">
                <a href="#"><img src="../images/cart.png" alt="shopping cart">5</a>
            </div>
        </div>
        <h1>Shopping Cart</h1>

        <!-- Start of the Dynamic Section -->
        <div class = "menu">
            <img src="../images/pizza.png" alt="Items Pic" class="img-circle" width="8%" height="8%">
            <h3>Categorys name</h3>
            <div class = "tab">
                <form action="../buy.php">
                    <table class = "table table-striped">
                        <thead>
                            <tr>
                                <th>Options</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Buy</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tomato &amp; Cheese</td>
                                <td>Small</td>
                                <td>9.75</td>
                                <td><input type="radio" name="item" value="wudbetype+size"></td>
                            </tr>
                            <tr>
                                <td>Tomato &amp; Cheese</td>
                                <td>Small</td>
                                <td>9.75</td>
                                <td><input type="radio" name="item" value=""></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class= "submit">
                        <input class="btn btn-success" type="submit" value="Buy!">
                    </div>
                </form>
            </div>
        </div>

        <div class="pagi">
            <ul class ="pager">
                <li><a href="#">Next</a></li>
                <li><a href="#">Previous</a></li>
            </ul>
        </div>
    </body>
    </html>
